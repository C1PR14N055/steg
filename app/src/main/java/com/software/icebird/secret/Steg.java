package com.software.icebird.secret;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

/**
 * Created by Ciprian on 03/11.
 */
public class Steg {

    private static final String SEPARATOR = "/";
    private static final String STEG_BOF = "ffx0";
    private static final String STEG_EOF = "0xff";

    static String stegImage(String path, String name, String outputDir, String message) {

        message = STEG_BOF + message + STEG_EOF;

        Bitmap image = BitmapFactory.decodeFile(path);
        int[] pixels = new int[image.getHeight() * image.getWidth()];
        image.getPixels(pixels, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());

        int[] newPixels = Arrays.copyOf(pixels, pixels.length);

        byte[] b = message.getBytes();

        int count = 0;
        for (int i = 0; i < b.length; i++) {
            byte current_byte = b[i];
            for (int j = 7; j >= 0; j--) {
                int lsb = (current_byte >> j) & 1;
                newPixels[count] = (pixels[count] & 0xfffffffe) + lsb;
                count++;
            }
        }

        Bitmap newImage = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
        newImage.setPixels(newPixels, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());

        File storageDir = null;
        FileOutputStream fos;
        try {
            storageDir = new File(outputDir + SEPARATOR + "Steg");
            storageDir.mkdirs();
            Log.d("STD", storageDir.getAbsolutePath() + SEPARATOR + name);
            fos = new FileOutputStream(storageDir + SEPARATOR + name);
            newImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return storageDir + SEPARATOR + name;
    }

    static String deStegImage(String path) {
        String message = "";
        Bitmap image = BitmapFactory.decodeFile(path);

        int[] pixels = new int[image.getHeight() * image.getWidth()];
        image.getPixels(pixels, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());

        int bit = 0;
        for (int pixel : pixels) {
            int ascii = 0;
            for (int j = 7; j >= 0; j--) {
                ascii += (pixels[bit] & 1) << j;
                bit++;
            }
            message += (char) ascii;
            if (message.length() >= STEG_BOF.length() && !message.startsWith(STEG_BOF)){
                message = null;
                break;
            }
            if (message.startsWith(STEG_BOF) && message.endsWith(STEG_EOF)) {
                message = message.substring(message.indexOf(STEG_BOF) + STEG_BOF.length(),
                        message.indexOf(STEG_EOF));
                break;
            }
        }

        return message;
    }
}