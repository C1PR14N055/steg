package com.software.icebird.secret;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    private static final int PERMISSION_REQ = 2;
    String imagePath, imageName, outputDir;

    EditText editText_message, editText_file;
    Button button_browse, button_exec;
    RadioGroup radioGroup;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent receivedIntent = getIntent();
        String receivedAction = receivedIntent.getAction();
        if (receivedAction.equals(Intent.ACTION_OPEN_DOCUMENT) ||
                receivedAction.equals(Intent.ACTION_SEND) ||
                receivedAction.equals(Intent.ACTION_VIEW) ||
                receivedAction.equals(Intent.ACTION_EDIT)) {
            receivedIntent.setAction("");
            Uri imageUri = receivedIntent.getData();
            if (imageUri == null) {
                ClipData.Item item = receivedIntent.getClipData().getItemAt(0);
                imageUri = item.getUri();
            }
            imagePath = getRealPathFromURI(MainActivity.this, imageUri);
            imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1, imagePath.length());
            setFilePathText(imageName);
            Log.d("INTENT_imagePath", imagePath);
        }
    }

    private void init() {

        outputDir = Environment.getExternalStorageDirectory().getAbsolutePath();

        editText_file = (EditText) findViewById(R.id.editText);
        editText_message = (EditText) findViewById(R.id.editText_message);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Please wait a little bit...");
        progress.setCancelable(false);

        button_browse = (Button) findViewById(R.id.button_browse);
        button_browse.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean needsRead = ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED;
                boolean needsWrite = ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED;
                if (needsRead || needsWrite) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_REQ);
                } else {
                    chooseImage();
                }
            }
        });

        button_exec = (Button) findViewById(R.id.button_exec);
        button_exec.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (imagePath.equals("")) {
                    toast("Please input image & secret message!", false);
                }
                if (radioGroup.getCheckedRadioButtonId() == R.id.radioButton_steg) {
                    showHideProgress(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final String stegImgPath = Steg.stegImage(imagePath, imageName, outputDir, editText_message.getText().toString());
                            addPicToGallery(MainActivity.this, stegImgPath);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    editText_message.setText("");
                                    showHideProgress(false);
                                    toast("Steg done.", false);
                                }
                            });
                        }
                    }).start();
                } else {
                    showHideProgress(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final String message = Steg.deStegImage(imagePath);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (message != null) {
                                        editText_message.setText(message);
                                        showHideProgress(false);
                                        toast("Desteg done.", false);
                                    } else {
                                        editText_message.setText("");
                                        showHideProgress(false);
                                        toast("No secrets in this image :(", true);
                                    }
                                }
                            });
                        }
                    }).start();
                }
            }
        });
    }

    private void setFilePathText(String text) {
        if (text.length() > 28) {
            text = text.substring(0, 13) + "..." + text.substring(text.length() - 13, text.length());
        }
        editText_file.setText(text);
    }

    private static void addPicToGallery(Context context, String photoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    private void chooseImage() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE);
    }

    private void showHideProgress(boolean show) {
        if (show) {
            progress.show();
        } else {
            progress.dismiss();
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void toast(String msg, boolean duration) {
        Toast.makeText(MainActivity.this, msg, (duration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQ) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseImage();
            } else {
                toast("App needs media permissions to function!", true);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri pickedImage = data.getData();
            imagePath = getRealPathFromURI(MainActivity.this, pickedImage);
            imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1, imagePath.length());
            Log.d("BROWSE_imagePath", imagePath);
            setFilePathText(imageName);
        }
    }
}